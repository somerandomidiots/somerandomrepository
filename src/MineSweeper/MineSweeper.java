package MineSweeper;

/**
 * Created by User on 28.07.2016.
 */
public class MineSweeper {
    private Slot[][] slot;
    private int bombs;
    private int fieldX;
    private int fieldY;


    public MineSweeper(int x, int y, int bombs){
        slot = new Slot[x][y];
        for (int i =0; i<x;i++){
            for (int j = 0; j<y;j++){
                slot[i][j]= new Slot();
            }
        }
        this.bombs = bombs;
        this.fieldX = x;
        this.fieldY = y;
        setBombs();
        for (int i =0; i<x;i++){
            for (int j = 0; j<y;j++){
                slot[i][j].setNeigbours(CountBomb(i,j));
            }
        }
    }

    private void setBombs(){
        for (int i = 0; i<bombs;i++) {
            int randomX = (int) (Math.random() * fieldX);
            int randomY = (int) (Math.random() * fieldY);
            if (slot[randomX][randomY]== null || slot[randomX][randomY].isBomb()){
                i--;
            } else {
                slot[randomX][randomY].setBomb(true);
            }

        }
    }


    public int CountBomb (int x, int y) {
        int bombCounter = 0;
        boolean up = (x - 1) >= 0;
        boolean down = (x + 1) < fieldX;
        boolean left = (y - 1) >= 0;
        boolean right = (y + 1) < fieldY;


        if (up && left) {
            if (slot[x-1][y-1].isBomb()) {
                bombCounter++;
            }
        }                   //oben links
        if (left) {
            if (slot[x][y - 1].isBomb()) {
                bombCounter++;
            }
        }                   //links
        if (down && left) {
            if (slot[x + 1][y - 1].isBomb()) {
                bombCounter++;
            }
        }                  //unten links
        if (down) {
            if (slot[x + 1][y].isBomb()) {
                bombCounter++;
            }
        }                   //unten
        if (down && right) {
            if (slot[x + 1][y + 1].isBomb()) {
                bombCounter++;
            }
        }          //unten rechts
        if (right) {
            if (slot[x][y + 1].isBomb()) {
                bombCounter++;
            }
        }                  //rechts
        if (up && right) {
            if (slot[x - 1][y + 1].isBomb()) {
                bombCounter++;
            }
        }            //oben rechts
        if (up) {
            if (slot[x - 1][y].isBomb()) {
                bombCounter++;
            }
        }                     //oben
        return bombCounter;
    }

    public boolean isBomb(int x, int  y){
        return slot[x][y].isBomb();
    }

    public int getNeigbors(int x, int y) {
        try{
            return slot[x][y].getNeigbours();
        }catch (ArrayIndexOutOfBoundsException e){
            return -1;
        }
    }

    public boolean getVisited(int x, int y) {
            boolean temp = slot[x][y].isVisited();
            if (!temp){
                slot[x][y].setVisited(true);
            }
            return temp;
    }

    public void setSlotStatus(int x, int y, Slot.StatusOfSlot statusofSlot){
        slot[x][y].setStatus(statusofSlot);
    }
}
