package MineSweeper;

/**
 * Created by User on 28.07.2016.
 */
public class Slot {



    public enum StatusOfSlot {
        OPEN,
        CLOSED,
        FLAGGED,
        QUESTIONED;
    }

    private int neigbours; //bombcount
    private boolean bomb;
    private StatusOfSlot status;
    private boolean visited;

    public Slot() {
        neigbours = 0;
        bomb = false;
        status = StatusOfSlot.CLOSED;
        visited = false;
    }

    public int getNeigbours() {
        return neigbours;
    }

    public boolean isBomb() {
        return bomb;
    }

    public void setNeigbours(int neigbours) {
        this.neigbours = neigbours;
    }

    public void setBomb(boolean bomb) {
        this.bomb = bomb;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setStatus(StatusOfSlot status) {
        this.status = status;
    }
}
