package MineSweeper;/**
 * Created by User on 28.07.2016.
 */

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.math.BigInteger;

public class GUI extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    public static final int FIELD_WIDTH = 100;
    public static final int FIELD_HEIGHT = 100;
    public static final int BOMB_COUNT = 30;
    private static final int SLOT_SIZE = 10;
    private MineSweeper mineSweeper = new MineSweeper(FIELD_WIDTH,FIELD_HEIGHT,BOMB_COUNT);
    private GraphicsContext gc;
    private int counterO;

    @Override
    public void start(Stage theStage) {
        theStage.setTitle("Canvas Example");

        Group root = new Group();
        Scene theScene = new Scene(root);
        theStage.setScene(theScene);

        Canvas canvas = new Canvas(SLOT_SIZE * FIELD_HEIGHT, SLOT_SIZE * FIELD_WIDTH);
        root.getChildren().add(canvas);

        gc = canvas.getGraphicsContext2D();
        drawSlots();
        fillSlots("X");
        theStage.show();

        theScene.setOnMouseClicked(
                new EventHandler<MouseEvent>(){
                    public void handle(MouseEvent e){
                        int x = (int) (e.getX()/SLOT_SIZE);
                        int y = (int) (e.getY()/SLOT_SIZE);
                        System.out.println("x"+x+"\ny"+y);
                        if (mineSweeper.isBomb(x,y)){
                            mineSweeper.setSlotStatus(x,y, Slot.StatusOfSlot.OPEN);
                            editSlot(x,y,"B");
                        }else if (mineSweeper.getNeigbors(x,y) == 0){
                            mineSweeper.setSlotStatus(x,y, Slot.StatusOfSlot.OPEN);
                            checkZero(x,y);
                        }else {
                            mineSweeper.setSlotStatus(x,y, Slot.StatusOfSlot.OPEN);
                            editSlot(x,y,mineSweeper.getNeigbors(x,y)+"");
                        }
                        System.out.println(counterO);
                    };
                }
        );

    }

    private void editSlot(int x, int y, String content){
        clearSlot(x,y);
        gc.setFill(Color.GRAY);
        gc.fillRect(x*SLOT_SIZE,y*SLOT_SIZE,SLOT_SIZE-1,SLOT_SIZE-1);
        gc.setFill(Color.BLACK);
        gc.fillText(content,x*SLOT_SIZE+SLOT_SIZE/4,(y+1)*SLOT_SIZE-SLOT_SIZE/4,SLOT_SIZE);
    }

    private void clearSlot(int x, int y){
        gc.clearRect(x*SLOT_SIZE+5,y*SLOT_SIZE+5,SLOT_SIZE-7,SLOT_SIZE-7);
    }

    private void drawSlots(){
        for (int i= 0;i<SLOT_SIZE*FIELD_WIDTH; i+= SLOT_SIZE){
            gc.strokeLine(0,i,SLOT_SIZE*FIELD_WIDTH,i);
        }
        for (int j = 0; j<SLOT_SIZE*FIELD_HEIGHT;j+=SLOT_SIZE){
            gc.strokeLine(j,0,j,SLOT_SIZE*FIELD_HEIGHT);
        }
    }

    private void fillSlots(String content){

        Font font = Font.font("Arial", SLOT_SIZE*0.75);
        gc.setFont(font);

        for (int i=0;i<FIELD_WIDTH;i++){
            for (int j=1;j<=FIELD_HEIGHT;j++){
                gc.fillText(content,i*SLOT_SIZE+SLOT_SIZE/4,j*SLOT_SIZE-SLOT_SIZE/4,SLOT_SIZE);
            }
        }
    }

    private void checkZero(int x, int y){
        counterO++;

        if (mineSweeper.getVisited(x,y)){
            return;
        }
        editSlot(x,y,"0");
            if (mineSweeper.getNeigbors(x-1,y-1)==0) {
                checkZero(x-1,y-1);
            }
            if (mineSweeper.getNeigbors(x,y - 1)==0) {
                checkZero(x,y-1);
            }
            if (mineSweeper.getNeigbors(x + 1,y - 1)==0) {
                checkZero(x+1,y-1);
            }
            if (mineSweeper.getNeigbors(x + 1,y)==0) {
                checkZero(x+1,y);
            }
            if (mineSweeper.getNeigbors(x + 1,y + 1)==0) {
                checkZero(x+1,y+1);
            }
            if (mineSweeper.getNeigbors(x,y + 1)==0) {
                checkZero(x,y+1);
            }
            if (mineSweeper.getNeigbors(x - 1,y + 1)==0) {
                checkZero(x-1,y+1);
            }
            if (mineSweeper.getNeigbors(x - 1,y)==0) {
                checkZero(x-1,y);
            }

    }

}

