package GameOfLifeFirstTry;

/**
 * Created by User on 27.07.2016.
 */
public class Cell {
    private boolean alive;
    private int neighbors;
    private boolean nextAlive;

    public Cell(){
        alive = false;
        neighbors = 0;
        nextAlive = false;
    }

    public boolean isAlive() {
        return alive;
    }

    public int getNeighbors() {
        return neighbors;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setNeighbors(int neighbors) {
        this.neighbors = neighbors;
    }

    public boolean isNextAlive() {
        return nextAlive;
    }

    public void setNextAlive(boolean nextAlive) {
        this.nextAlive = nextAlive;
    }

    public void rules() {
        if (isAlive()) {
            if (getNeighbors() == 2 || getNeighbors() == 3) {
                setNextAlive(true);
                return;
            }
            setNextAlive(false);
        } else {
            if (getNeighbors() == 3) {
                setNextAlive(true);
            }else {
                setNextAlive(false);
            }
        }
    }

    public void updateStatus() {
        setAlive(isNextAlive());
    }

}
