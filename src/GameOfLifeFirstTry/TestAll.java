package GameOfLifeFirstTry; /**
 * Created by User on 28.07.2016.
 */

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.StackPaneBuilder;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

public class TestAll extends Application {
    public static final int CELL_SIZE = 5;
    private GameOfLife gameOfLife = new GameOfLife();
    private List<List<StackPane>> list = new ArrayList<>();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        initializeField();
        Pane root = new Pane();
        Scene scene = new Scene(root, GameOfLife.MAX_X*CELL_SIZE, GameOfLife.MAX_Y*CELL_SIZE);
        scene.getStylesheets().add("GameOfLifeFirstTry/gol.css");
        for (int i = 0; i< GameOfLife.MAX_X; i++){
            list.add(new ArrayList<>());
            for (int j = 0; j< GameOfLife.MAX_Y; j++){
                StackPane cell = StackPaneBuilder.create().layoutX(i*CELL_SIZE).layoutY(j*CELL_SIZE).prefHeight(CELL_SIZE).prefWidth(CELL_SIZE).styleClass("dead").build();
                root.getChildren().add(cell);
                list.get(i).add(cell);
            }
        }
        primaryStage.setTitle("Game of Life");
        primaryStage.setScene(scene);
        primaryStage.show();
        Timeline timeline = new Timeline(new KeyFrame(Duration.ZERO, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
              iterateField();
            }
        }),new KeyFrame(Duration.millis(100)));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    private void iterateField(){
        gameOfLife.countNeigborsAll();
        gameOfLife.rulesAll();
        gameOfLife.updateAll();
        for (int i = 0; i< GameOfLife.MAX_X; i++){
            for (int j = 0; j< GameOfLife.MAX_Y; j++){
                StackPane pane = list.get(i).get(j);
                pane.getStyleClass().clear();
                pane.getStyleClass().add(gameOfLife.getFieldStatus(i,j)? "alive":"dead");
            }
        }
    }

    private void initializeField(){
        for (int i = 0; i< GameOfLife.MAX_X; i++){
            for (int j = 0; j< GameOfLife.MAX_Y; j++){
                gameOfLife.setCell(i,j,Math.random()<0.3);
            }
        }
    }


}
