package GameOfLifeFirstTry;

/**
 * Created by User on 26.07.2016.
 */
public class GameOfLife{

    //x= zeile y= spalte

    public static final int MAX_X = 150;
    public static final int MAX_Y = 150;

    private Cell[][] field;

    public GameOfLife(){
        field = new Cell[MAX_X][MAX_Y];
        for (int i =0; i<MAX_X;i++){
            for (int j = 0; j<MAX_Y;j++){
                field[i][j]= new Cell();
            }
        }
    }

    public void countNeigborsAll() {
        for (int x = 0; x < MAX_X; x++) {
            for (int y = 0; y < MAX_Y; y++) {
                field[x][y].setNeighbors(countNeigbors(x,y));
            }
        }
    }

    private int countNeigbors(int x, int y) {
        int counter = 0;
        boolean up = (x - 1) >= 0;
        boolean down = (x + 1) < MAX_X;
        boolean left = (y - 1) >= 0;
        boolean right = (y + 1) < MAX_Y;


        if (up && left) {
            if (checkNeigbor(x - 1, y - 1)) {
                counter++;
            }
        }                   //oben links
        if (left) {
            if (checkNeigbor(x, y - 1)) {
                counter++;
            }
        }                   //links
        if (down && left) {
            if (checkNeigbor(x + 1, y - 1)) {
                counter++;
            }
        }                  //unten links
        if (down) {
            if (checkNeigbor(x + 1, y)) {
                counter++;
            }
        }                   //unten
        if (down && right) {
            if (checkNeigbor(x + 1, y + 1)) {
                counter++;
            }
        }          //unten rechts
        if (right) {
            if (checkNeigbor(x, y + 1)) {
                counter++;
            }
        }                  //rechts
        if (up && right) {
            if (checkNeigbor(x - 1, y + 1)) {
                counter++;
            }
        }            //oben rechts
        if (up) {
            if (checkNeigbor(x - 1, y)) {
                counter++;
            }
        }                     //oben
        return counter;
    }

    public boolean checkNeigbor(int x, int y) {
        return field[x][y].isAlive();
    }

    public void rulesAll() {
        for (int x = 0; x < MAX_X; x++) {
            for (int y = 0; y < MAX_Y; y++) {
                field[x][y].rules();
            }
        }
    }

    public void updateAll() {
        for (int x = 0; x < MAX_X; x++) {
            for (int y = 0; y < MAX_Y; y++) {
                field[x][y].updateStatus();
            }
        }
    }

    public static int getMAXX() {
        return MAX_X;
    }

    public static int getMaxY() {
        return MAX_Y;
    }

    public Cell[][] getField() {
        return field;
    }

    public boolean getFieldStatus(int x, int y) {
        return field[x][y].isAlive();
    }

    public void setCell(int x, int y, boolean status){
        field[x][y].setAlive(status);
    }
}





/*
2 matrix (aka 2D array), size= x*x x= freie wahl(?), x=5 zum testen?
status: D (dead) und A(alive)?.

loop beginn
go:
(x|y) nachbar status zählen: (x-1|y) (x+1,y)  (x,y-1)  (x,y+1) - (x-1,y-1)  (x+1,y+1)  (x+1,y-1)  (x-1,y+1)
 anzahl D=d, anzahl A=a (nur alive zählen!!)
 (note: koordinate 0 bzw -1 vermeiden! nicht außerhalb der matrix gehen!)

status von (x|y) speichern

 1. if alive AND (A<2) --> dead
 2. if alive AND(A=2 or A=3) --> alive
 3. if alive AND (A>3) --> dead
 4. if dead AND (A=3) --> alive
 ==> neuen status in der ANDEREN (nicht aktuell benutzten) matrix speichern für nächste generation

  gleiches nochmal mit der neuen/anderen matrix

  loop ende
 */