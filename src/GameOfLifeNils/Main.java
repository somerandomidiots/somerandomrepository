package GameOfLifeNils;/**
 * Created by Braveheart on 02.08.2016.
 */

import com.sun.istack.internal.NotNull;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.PixelWriter;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Main extends Application {

    private final int WIDTH = 800;
    private final int HEIGHT = 640;

    public static void main(String[] args) {
        launch(args);
    }

    GameOfLife gameOfLife = new GameOfLife(WIDTH, HEIGHT);

    @Override
    public void start(@NotNull final Stage primaryStage) {
        primaryStage.setTitle("Game of Life");
        gameOfLife.randomFill(0.3);
        BorderPane root = new BorderPane();
        Canvas canvas = new Canvas(WIDTH, HEIGHT);
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        Pane layerPane = new Pane();
        layerPane.getChildren().addAll(canvas);
        root.setCenter(layerPane);
        Scene scene = new Scene(root, WIDTH, HEIGHT);
        primaryStage.setScene(scene);
        primaryStage.show();
        startAnimation(graphicsContext);
    }

    private void startAnimation(@NotNull final GraphicsContext graphicsContext) {
        PixelWriter pixelWriter = graphicsContext.getPixelWriter();
        AnimationTimer loop = new AnimationTimer() {
            @Override
            public void handle(long now) {
                for (int x = 0; x < WIDTH; ++x) {
                    for (int y = 0; y < HEIGHT; ++y) {
                        pixelWriter.setColor(x, y, gameOfLife.isAlive(x, y) ? Color.BLACK : Color.WHITE);
                    }
                }
                gameOfLife.update();
            }
        };
        loop.start();
    }
}
