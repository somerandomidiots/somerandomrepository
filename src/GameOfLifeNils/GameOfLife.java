package GameOfLifeNils;

import com.sun.istack.internal.NotNull;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Braveheart on 02.08.2016.
 */
public class GameOfLife {

    private static final Random RANDOM = new Random();

    private enum Direction {
        NORTH(0, -1),
        NORTHEAST(1, -1),
        EAST(1, 0),
        SOUTHEAST(1, 1),
        SOUTH(0, 1),
        SOUTHWEST(-1, 1),
        WEST(-1, 0),
        NORTHWEST(-1, -1);

        private Direction(final int deltaX, final int deltaY) {
            this.deltaX = deltaX;
            this.deltaY = deltaY;
        }

        private final int deltaX;
        private final int deltaY;
    }

    public enum Border {
        DEAD {
            boolean isNeighbourAlive(@NotNull final GameOfLife gol, final int x, final int y, @NotNull final Direction direction) {
                final int neighbourX = x + direction.deltaX;
                final int neighbourY = y + direction.deltaY;
                return neighbourX >= 0 && neighbourY >= 0 && neighbourX < gol.WIDTH && neighbourY < gol.HEIGHT && gol.field[neighbourX][neighbourY];
            }
        },
        ALIVE {
            boolean isNeighbourAlive(@NotNull final GameOfLife gol, final int x, final int y, @NotNull final Direction direction) {
                final int neighbourX = x + direction.deltaX;
                final int neighbourY = y + direction.deltaY;
                return neighbourX < 0 || neighbourY < 0 || neighbourX >= gol.WIDTH || neighbourY >= gol.HEIGHT || gol.field[neighbourX][neighbourY];
            }
        },
        WRAP {
            boolean isNeighbourAlive(@NotNull final GameOfLife gol, final int x, final int y, @NotNull final Direction direction) {
                final int neighbourX = x + direction.deltaX;
                final int neighbourY = y + direction.deltaY;
                return gol.field[(neighbourX + gol.WIDTH) % gol.WIDTH][(neighbourY + gol.HEIGHT) % gol.HEIGHT];
            }
        };

        abstract boolean isNeighbourAlive(@NotNull final GameOfLife gol, final int x, final int y, @NotNull final Direction direction);
    }

    private boolean[][] field;
    private final int HEIGHT;
    private final int WIDTH;
    private final boolean[] newLife;
    private final boolean[] stayAlive;
    private final Border border;

    public GameOfLife(final int width, final int height) {
        this(width, height, new boolean[]{
                        false, // 0
                        false, // 1
                        false, // 2
                        true,  // 3
                        false, // 4
                        false, // 5
                        false, // 6
                        false, // 7
                        false  // 8
                }, new boolean[]{
                        false, // 0
                        false, // 1
                        true,  // 2
                        true,  // 3
                        false, // 4
                        false, // 5
                        false, // 6
                        false, // 7
                        false  // 8
                }, Border.DEAD);
    }

    public GameOfLife(final int width, final int height, @NotNull final boolean[] newLife, @NotNull final boolean[] stayAlive, @NotNull final Border border) {
        if (width < 0) {
            throw new IllegalArgumentException("Illegal bounds: width(" + width + ")[0,]");
        }
        if (height < 0) {
            throw new IllegalArgumentException("Illegal bounds: height(" + height + ")[0,]");
        }
        if (newLife.length > 9) {
            throw new IllegalArgumentException("Illegal rules: newLife.length(" + newLife.length + ")[9]");
        }
        if (stayAlive.length > 9) {
            throw new IllegalArgumentException("Illegal rules: stayAlive.length(" + stayAlive.length + ")[9]");
        }
        this.HEIGHT = height;
        this.WIDTH = width;
        field = new boolean[width][height];
        for (boolean[] column : field) {
            Arrays.fill(column, false);
        }
        this.newLife = Arrays.copyOf(newLife, 9);
        this.stayAlive = Arrays.copyOf(stayAlive, 9);
        this.border = border;
    }

    public void randomFill(final double chance) {
        for (boolean[] column : field) {
            for (int y = 0; y < HEIGHT; ++y) {
                column[y] = RANDOM.nextDouble() < chance;
            }
        }
    }

    private boolean getNeighbour(final int x, final int y, @NotNull final Direction direction) {
        final int neighbourX = x + direction.deltaX;
        final int neighbourY = y + direction.deltaY;
        return neighbourX >= 0 && neighbourY >= 0 && neighbourX < WIDTH && neighbourY < HEIGHT && field[neighbourX][neighbourY];
    }

    public void update() {
        final boolean[][] newField = new boolean[WIDTH][HEIGHT];
        for (int x = 0; x < WIDTH; ++x) {
            for (int y = 0; y < HEIGHT; ++y) {
                int countNeighbours = 0;
                for (Direction direction : Direction.values()) {
                    if (border.isNeighbourAlive(this, x, y, direction)) {
                        ++countNeighbours;
                    }
                }
                newField[x][y] = (field[x][y] ? stayAlive : newLife)[countNeighbours];
            }
        }
        field = newField;
    }

    public void setAlive(final int x, final int y, final boolean alive) {
        field[x][y] = alive;
    }

    public boolean isAlive(final int x, final int y) {
        return field[x][y];
    }
}
